const mongoose = require('mongoose');
module.exports = mongoose.model('User', {
	email: {
		type: String,
		required: true,
	},
	created_date: {
		type: String,
		default: new Date()
	},
});
