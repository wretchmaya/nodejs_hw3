const mongoose = require('mongoose');
module.exports = mongoose.model('Credentials', {
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
});
