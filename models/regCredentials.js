const mongoose = require('mongoose');
module.exports = mongoose.model('RegistrationCredentials', {
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	role: {
		type: String,
		required: true
	},
});
