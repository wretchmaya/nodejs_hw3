const express = require('express');
const route = express.Router();


const { login, register } = require('../controllers/authController');

route.post('/auth/register', register);
route.post('/auth/login', login)

module.exports = route;