const express = require('express');
const route = express.Router();

const { getInfo, deleteUser, updatePassword } = require('../controllers/userController');
const authMiddleware = require('../middlewares/authMiddleware');

route.get('/users/me', authMiddleware, getInfo);
route.delete('/users/me', authMiddleware, deleteUser);
route.patch('/users/me/password', authMiddleware, updatePassword);

module.exports = route;
