const express = require('express');
const app = express();
const { PORT } = require('./config/serverConfig');
app.use(express.json());

const mongoose = require('mongoose');
const { DB_NAME, DB_PASSWORD, DB_USER_NAME } = require('./config/databaseConfig');
mongoose.connect(
	`mongodb+srv://${DB_USER_NAME}:${DB_PASSWORD}@cluster0.ocz36.mongodb.net/${DB_NAME}?retryWrites=true&w=majority`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
		useFindAndModify: false,
		useCreateIndex: true,
	}
);

const authRotes = require('./routes/authRoute');
const userRoute = require('./routes/userRoute');

app.use('/api', authRotes);
app.use('/api', userRoute);

app.listen(PORT, () => {
	console.log(`Server is running on port ${PORT}`);
});
