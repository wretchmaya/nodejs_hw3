const User = require('../models/user');
const Credentials = require('../models/credentials');
const regCredentials = require('../models/regCredentials');
module.exports.getInfo = async (req, res) => {
	try {
		console.log(req.user, 'from get info module');
		User.findById(req.user.userId)
			.exec()
			.then(user => {
				if (!user) {
					return res.status(400).json({ message: 'No such user has been found' });
				}
				res.status(200).json({
					user: {
						_id: user._id,
						email: user.email,
						created_date: user.created_date,
					},
				});
			});
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
};
module.exports.deleteUser = async (req, res) => {
	try {
		console.log(req.user, 'from delete user module');
		await Promise.all([
			User.deleteOne({ _id: req.user.userId }),
			Credentials.deleteOne({ email: req.user.email }),
			regCredentials.deleteOne({ email: req.user.email }),
		]);
		return res.status(200).json({ message: 'Profile deleted successfully' });
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
};
module.exports.updatePassword = async (req, res) => {
	try {
		const { oldPassword, newPassword } = req.body;
		if (!oldPassword || !newPassword) {
			return res
				.status(400)
				.json({ message: 'Old password or new password field is missing' });
		}
		const user = await User.findById(req.user.userId);
		if (!user) {
			return res.status(400).json({ message: 'Wrong user id' });
		}
		const userCredentials = await Credentials.findOne({ email: user.email });
		if (oldPassword === userCredentials.password) {
			await Credentials.findOneAndUpdate(
				{ email: userCredentials.email },
				{ $set: { password: newPassword } }
			);
			return res.status(200).json({ message: 'Password changed successfully' });
		}
		return res.status(400).json({ message: 'Wrong password' });
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
};
