const jwt = require('jsonwebtoken');
const { secret } = require('../config/secret');

const Creadentials = require('../models/credentials');
const User = require('../models/user');
const RegistrationCredentials = require('../models/regCredentials');

module.exports.register = async (req, res) => {
	try {
		const { email, password, role } = req.body;
		if (!password) return res.status(400).json({ message: 'Password is required' });
		if (!email) return res.status(400).json({ message: 'Email is required' });
		if (!role) return res.status(400).json({ message: 'Role is required' });

		const user = new User({
			email,
		}).save();
		const credentials = new Creadentials({
			email,
			password,
		}).save();
		const regCredentials = new RegistrationCredentials({
			email,
			password,
			role,
		}).save();
		await Promise.all([user, credentials, regCredentials]);
		res.status(200).json({ message: 'Profile created successfully' });
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
};
module.exports.login = async (req, res) => {
	try {
		const { email, password } = req.body;
		if (!password) return res.status(400).json({ message: 'Password is required' });
		if (!email) return res.status(400).json({ message: 'Email is required' });

		const [user, credentials] = await Promise.all([
			await User.findOne({ email }),
			await Creadentials.findOne({ email }),
		]);
		if (user && password === credentials.password) { 
			const token = jwt.sign(
				{
					email: user.email,
					userId: user._id,
					created_date: user.created_date,
					credentialsId: credentials._id,
				},
				secret
			);
			return res.status(200).json({ message: 'Succsess', jwt_token: token });
		}
		return res.status(400).json({message: 'Wrong password or email'})
	} catch (e) {
		res.status(500).json({ message: e.message });
	}
};
