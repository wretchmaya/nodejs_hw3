const jwt = require('jsonwebtoken');
const { secret } = require('../config/secret')


module.exports = (req, res, next) => {
	const authHeader = req.headers['authorization'];

	if (!authHeader) {
		return res.status(401).json({ status: 'No authorization header' });
	}

	const [, token] = authHeader.split(' ');

	try {
		req.user = jwt.verify(token, secret);
		next();
	} catch (error) {
		res.status(401).json({ status: 'Problems with token have been occured' });
	}
};